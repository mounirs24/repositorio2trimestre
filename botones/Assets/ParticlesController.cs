using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesController : MonoBehaviour
{
    public ParticleSystem ParticleSystem;


    public void switchoff ()
    {
        var emission = ParticleSystem.emission;
        emission.enabled = false;

    }
    public void switchon ()
    {
        var emission = ParticleSystem.emission;
        emission.enabled = true;
    }

    public void suma (int incremento)
    {
        var emission = ParticleSystem.emission;
        emission.rateOverTime = emission.rateOverTime.constant + incremento;
    }
}